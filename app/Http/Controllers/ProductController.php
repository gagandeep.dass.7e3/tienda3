<?php

namespace App\Http\Controllers;


use App\Models\Cart;
use App\Models\Cart_Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\Product;




class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show(Request $request)
    {
        $value = empty($request->input('precio')) ? '0' : $request->input('precio');

        $all = DB::select(DB::raw("select * from products where precio > $value"));

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($all);
        $perPage = 5;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $products = new LengthAwarePaginator($currentPageItems, count($itemCollection), $perPage);
        $products->setPath($request->url());

        return view('products')->with('products', $products);

    }

    public function addToCart($id)
    {
        $product = Product::find($id);
        /*Log::error('error');
        Log::warning('warning');
        Log::info('info');*/
        $user = Auth::user()->id;
        //dd($product);
        $cart_db = Cart::where("user_id",$user)->first();

        if (empty($cart_db)){
            $cart_db = new Cart();
            $cart_db->user_id = $user;
            $cart_db->save();
        }

        if (!$product) {
            abort(404);
        }
        $cart = session()->get('cart');

        if (!$cart) {
            $cart = [
                $id => [
                    'nombre' => $product->nombre,
                    'cantidad' => 1,
                    'precio' => $product->precio,
                    'image' => $product->image,
                ]
            ];

            $cart_items = new Cart_item();
            $cart_items->cart_id=$cart_db->id;
            $cart_items->product_id=$id;
            $cart_items->cantidad = 1;
            $cart_items->save();

            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Producto añadido correctamente');


        }
        if (isset($cart[$id])) {
            $cart[$id]['cantidad']++;
            $cart_items = Cart_Item::where([['cart_id',$cart_db->id], ['product_id',$id]])->first();
            $cart_items->cantidad=$cart[$id]['cantidad'];
            $cart_items->update();
            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }

        $cart[$id] = [
            'nombre' => $product->nombre,
            'cantidad' => 1,
            'precio' => $product->precio,
            'image' => $product->image,
        ];
        $cart_items = new Cart_Item();
        $cart_items->cart_id=$cart_db->id;
        $cart_items->product_id=$id;
        $cart_items->cantidad=1;
        $cart_items->save();

        session()->put('cart', $cart);
        return redirect()->back()->with('success', 'Product added to cart successfully!');
    }

    public function update(Request $request)
    {
        if ($request->id and $request->cantidad) {
            $cart = session()->get('cart');
            $cart[$request->id]["cantidad"] = $request->cantidad;
            $user = Auth::user()->id;
            $cart_db = Cart::where('user_id',$user)->first();
            $cart_items = Cart_Item::where([['cart_id',$cart_db->id],['product_id',$request->id]])->first();
            $cart_items->cantidad=$cart[id]['cantidad'];
            $cart_items->cantidad=$request->cantidad;
            $cart_items->update();
            session()->put('cart', $cart);
            session()->flash('success', 'Cart updated successfully');
        }
    }

    public function remove(Request $request)
    {
        if ($request->id) {
            $cart = session()->get('cart');
            if (isset($cart[$request->id])) {
                unset($cart[$request->id]);
                $user = Auth::user()->id;
                $cart_db = Cart::where('user_id',$user)->first();
                $cart_items = Cart_Item::where([['cart_id',$cart_db->id],['product_id',$request->id]])->first();
                $cart_items->delete();
                session()->put('cart', $cart);
            }
            session()->flash('success', 'Product removed successfully');
        }
    }

    public function venta(){
        $products = DB::table('cart_items')->get();
        for ($i=0;$i<$products->count();$i++){
            $id=$products[$i]->product_id;
            $cantidad=$products[$i]->cantidad;
            $cantidadStock=DB::table('products')
                ->where('id', $id)
                ->where('stock', '>=', $cantidad)
                ->decrement('stock', $cantidad);
            if($cantidadStock==0){
                return Redirect::back()->withErrors(['Producto sin stock', 'Producto sin stock']);
            }
        }
        DB::table('cart_items')->where('id', '>=', 0)->delete();
        session()->remove('cart');
        return redirect()->back()->with('success', 'Tu compra se ha realizado con exito');
    }


    public function cart()
    {
        return view('cart');
    }
}
