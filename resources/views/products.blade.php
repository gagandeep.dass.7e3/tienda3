@extends('layouts.app')

@section('content')

    <style>
        .btn-carrito{
            font-weight: 600;
            margin-top: 1rem;
        }
        .btn-carrito:hover{
            color: green;
        }
        .filtro-precio{
            margin-left: 4rem;
            margin-top: 1rem;
        }
    </style>

    <div class="container">
        <div class="card-header filtro-precio">
            <form method="get" action="/products">
                <input name="precio" type="text" placeholder="Filtro Precio" class="rounded m-2">
                <input name="buscar" type="submit" style="color:green; padding:5px; border:1px solid green;" >
            </form>
            <div class="btn-carrito"><a href="{{url('cart/')}}">Ir al Carrito</a></div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header" style="margin-left: 2rem; margin-top:2rem; font-size:1.5rem;">{{ __('Products') }}</div>
                    <div class="card-body p-5" style="display:flex; flex-flow: row wrap; margin:10px">
                        @foreach ($products as $product)
                            <div class="shadow p-3 mb-5 bg-white" style="margin:10px; width: 15rem;">
                                <img style="width: 10rem; display:block; margin:auto;" src="data:image/jpeg;base64,{!! stream_get_contents($product->image) !!}"/>
                                <ul class="list-group m-2">
                                    <li style="font-weight: 600;" class="list-group-item">{{$product->nombre}} </li>
                                    <li class="list-group-item">{{$product->precio}}€</li>
                                </ul>
                                <p class="btn-holder "><a href="{{ url('add-to-cart/'.$product->id) }}" class="btn btn-warning btn-block text-center border p-1" role="button" style="border:2px solid springgreen;display:block; margin:auto;">Añadir al carrito</a> </p>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        {{$products->links()}}
    </div>
@endsection
