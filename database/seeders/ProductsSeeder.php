<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'nombre'=>'Iphone 8',
            'precio'=>1.99,
            'stock'=>2,
            'descripcion'=>'description product',
            'image'=>base64_encode(file_get_contents('/home/itb/Escritorio/product.png'))
        ]);

        DB::table('products')->insert([
            'nombre'=>'Samsung A20',
            'precio'=>5.99,
            'stock'=>2,
            'descripcion'=>'description product',
            'image'=>base64_encode(file_get_contents('/home/itb/Escritorio/product.png'))
        ]);

        DB::table('products')->insert([
            'nombre'=>'Motorola',
            'precio'=>14.99,
            'stock'=>2,
            'descripcion'=>'description product',
            'image'=>base64_encode(file_get_contents('/home/itb/Escritorio/product.png'))
        ]);

        DB::table('products')->insert([
            'nombre'=>'Pc Gaming RTX 3080TI',
            'precio'=>25.9,
            'stock'=>2,
            'descripcion'=>'description product',
            'image'=>base64_encode(file_get_contents('/home/itb/Escritorio/product.png'))
        ]);

        DB::table('products')->insert([
            'nombre'=>'GTX 1080TI',
            'precio'=>59.99,
            'stock'=>10,
            'descripcion'=>'description product',
            'image'=>base64_encode(file_get_contents('/home/itb/Escritorio/product.png'))
        ]);

        DB::table('products')->insert([
            'nombre'=>'Ryzen 5 2600',
            'precio'=>150.99,
            'stock'=>10,
            'descripcion'=>'description product',
            'image'=>base64_encode(file_get_contents('/home/itb/Escritorio/product.png'))
        ]);

        DB::table('products')->insert([
            'nombre'=>'PS5',
            'precio'=>450,
            'stock'=>8,
            'descripcion'=>'description product',
            'image'=>base64_encode(file_get_contents('/home/itb/Escritorio/product.png'))
        ]);

        DB::table('products')->insert([
            'nombre'=>'Headphones Apple',
            'precio'=>600,
            'stock'=>12,
            'descripcion'=>'description product',
            'image'=>base64_encode(file_get_contents('/home/itb/Escritorio/product.png'))
        ]);
    }
}
